// Bài 1: Tìm số nguyên dương nhỏ nhất

function timSoNguyenDuong() {
  var soNguyenDuong = 1;
  var sum = 1;
  while (sum <= 10000) {
    soNguyenDuong++;
    sum += soNguyenDuong;
  }
  document.getElementById("inKetQua").innerHTML = soNguyenDuong;
}

// Bài 2: Viết chương trình nhập vào 2 số x và n. Tính tổng: S(n) = x + x^2
// + … + x^n
document.getElementById("tinhTong").onclick = function () {
  var soX = document.getElementById("numX").value * 1;
  var soN = document.getElementById("numN").value * 1;
  var sum = 0;
  var tong;

  for (var i = 1; i <= soN; i++) {
    tong = Math.pow(soX, i);
    sum += tong;
  }
  document.getElementById("inKetQua1").innerHTML = `Tổng là: ` + sum;
};

// Bài 3: Tính giai thừa

document.getElementById("btnTinhGiaiThua").onclick = function () {
  var soGiaiThua = document.getElementById("nhapSo").value * 1;
  var giaiThua = 1;

  for (var i = 1; i <= soGiaiThua; i++) {
    giaiThua = giaiThua * i;
  }
  document.getElementById("inKetQua2").innerHTML = `kết quả là: ` + giaiThua;
};




// Bài 4: Tạo thẻ div

document.getElementById("btnTaoTheDiv").onclick = function () {
  var Number = document.getElementById("nhapSoTheDiv").value * 1;
  var divChanLe = "";
  

  for (var i = 1; i <= Number; i++) {
    if (i % 2 == 0) {
      divChanLe = divChanLe + `<div class="bg-danger mt-2"> div chan ${i}</div>`;
    } else {
      divChanLe = divChanLe + `<div class="bg-primary mt-2"> div le ${i}</div>`;
    }
  }
  document.getElementById("inKetQua3").innerHTML = divChanLe;
};
